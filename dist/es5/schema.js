"use strict";
var _ = require('lodash');
var composed_validation_result_1 = require('./composed-validation-result');
var root_validator_1 = require('./validators/root-validator');
var Schema = (function () {
    function Schema(schema, options) {
        if (options === void 0) { options = {}; }
        this.schema = schema;
        this.options = options;
        _.defaults(options, Schema.DefaultOptions);
    }
    Schema.prototype.validateKey = function (value, key, object, options) {
        var rootValidator = new root_validator_1.RootValidator();
        var definition = this.schema[key];
        if (definition.type instanceof Function || definition.type instanceof Schema || _.isObject(definition.type)) {
            var result = rootValidator.validate(key, definition, value, options);
            if (typeof definition.custom === 'function') {
                var custom = definition.custom(value, object, options.context);
                if (custom) {
                    result.and({
                        property: key,
                        rule: 'custom',
                        message: custom
                    });
                }
            }
            else if (typeof definition.custom === 'object') {
                for (var rule in definition.custom) {
                    if (definition.custom.hasOwnProperty(rule)) {
                        var custom = definition.custom[rule](value, object, options.context);
                        if (custom) {
                            result.and({
                                property: key,
                                rule: rule,
                                message: custom
                            });
                        }
                    }
                }
            }
            return result;
        }
        else {
            throw new Error("Invalid type '" + definition.type + "' used in " + this.name);
        }
    };
    Schema.prototype.cleanKey = function (key, object, options) {
        if (options === void 0) { options = {}; }
        var rootValidator = new root_validator_1.RootValidator();
        var definition = this.schema[key];
        if (definition.type instanceof Function || definition.type instanceof Schema || _.isObject(definition.type)) {
            return rootValidator.clean(definition, object[key], options, object);
        }
        else {
            throw new Error("Invalid type '" + definition.type + "' used in " + this.name);
        }
    };
    Schema.prototype.validate = function (object, key, options) {
        if (typeof options === 'undefined' && _.isObject(key)) {
            options = key;
        }
        options = _.defaults(options || {}, Schema.DefaultOptions);
        if (typeof key !== 'string') {
            if (options.clean) {
                var cleanOptions = _.defaults(options.clean, Schema.DefaultCleanOptions);
                object = this.clean(object, cleanOptions);
            }
            var result = new composed_validation_result_1.ComposedValidationResult();
            for (var key_1 in this.schema) {
                if (this.schema.hasOwnProperty(key_1)) {
                    result.and(this.validateKey(object[key_1], key_1, object, options));
                }
            }
            return result;
        }
        else {
            if (options.clean) {
                var cleanOptions = _.defaults(options.clean, Schema.DefaultCleanOptions);
                object = this.cleanKey(key, object, cleanOptions);
            }
            var result = this.validateKey(object[key], key, object, options);
            return result.isValid() ? null : result;
        }
    };
    Schema.prototype.clean = function (object, options) {
        if (options === void 0) { options = {}; }
        if (typeof object === 'undefined' || object === null) {
            return object;
        }
        _.defaults(options, Schema.DefaultCleanOptions);
        var result = options.mutate ? object : _.cloneDeep(object);
        for (var key in this.schema) {
            if (this.schema.hasOwnProperty(key)) {
                result[key] = this.cleanKey(key, object, options);
            }
        }
        return result;
    };
    Schema.prototype.extend = function (schema) {
        return this;
    };
    Schema.prototype.getValidators = function (key, object, options) {
        if (typeof key === 'string') {
            options = typeof options === 'object' ? _.defaults(options, this.options) : this.options;
            return root_validator_1.RootValidator.getValidatorsForKey(key, this.schema[key], options, object);
        }
        else {
            options = object;
            object = key;
            options = typeof options === 'object' ? _.defaults(options, this.options) : this.options;
            var validators = {};
            for (var key_2 in this.schema) {
                if (this.schema.hasOwnProperty(key_2)) {
                    var keyValidators = {};
                    _.assign(keyValidators, root_validator_1.RootValidator.getValidatorsForKey(key_2, this.schema[key_2], options, object));
                    validators[key_2] = keyValidators;
                }
            }
            return validators;
        }
    };
    Schema.DefaultOptions = {
        name: 'Schema',
        clean: false,
        strict: false,
        context: {}
    };
    Schema.DefaultCleanOptions = {
        mutate: false,
        trimStrings: true,
        removeEmptyStrings: true,
        removeEmptyObjects: true,
        rounding: 'round',
        filter: false,
        autoConvert: true,
        getAutoValues: true
    };
    Schema.RegEx = {
        Email: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
    };
    return Schema;
}());
exports.Schema = Schema;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjaGVtYS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsSUFBWSxDQUFDLFdBQU0sUUFFbkIsQ0FBQyxDQUYwQjtBQVEzQiwyQ0FBd0MsOEJBQ3hDLENBQUMsQ0FEcUU7QUFDdEUsK0JBQThCLDZCQUU5QixDQUFDLENBRjBEO0FBRTNEO0lBMEJJLGdCQUFzQixNQUFXLEVBQVUsT0FBK0I7UUFBdkMsdUJBQXVDLEdBQXZDLFlBQXVDO1FBQXBELFdBQU0sR0FBTixNQUFNLENBQUs7UUFBVSxZQUFPLEdBQVAsT0FBTyxDQUF3QjtRQUN0RSxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUE7SUFDOUMsQ0FBQztJQUVPLDRCQUFXLEdBQW5CLFVBQW9CLEtBQVUsRUFBRSxHQUFXLEVBQUUsTUFBVyxFQUFFLE9BQTBCO1FBQ2hGLElBQU0sYUFBYSxHQUFHLElBQUksOEJBQWEsRUFBRSxDQUFBO1FBQ3pDLElBQU0sVUFBVSxHQUF5QixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBeUIsQ0FBQTtRQUVqRixFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxZQUFZLFFBQVEsSUFBSSxVQUFVLENBQUMsSUFBSSxZQUFZLE1BQU0sSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUcsSUFBTSxNQUFNLEdBQUcsYUFBYSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxPQUFPLENBQTZCLENBQUE7WUFDbEcsRUFBRSxDQUFDLENBQUMsT0FBTyxVQUFVLENBQUMsTUFBTSxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUM7Z0JBQzFDLElBQUksTUFBTSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUE7Z0JBQzlELEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ1QsTUFBTSxDQUFDLEdBQUcsQ0FBQzt3QkFDUCxRQUFRLEVBQUUsR0FBRzt3QkFDYixJQUFJLEVBQUUsUUFBUTt3QkFDZCxPQUFPLEVBQUUsTUFBTTtxQkFDbEIsQ0FBQyxDQUFBO2dCQUNOLENBQUM7WUFDTCxDQUFDO1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sVUFBVSxDQUFDLE1BQU0sS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUMvQyxHQUFHLENBQUMsQ0FBQyxJQUFJLElBQUksSUFBSSxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDakMsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUN6QyxJQUFJLE1BQU0sR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFBO3dCQUNwRSxFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDOzRCQUNULE1BQU0sQ0FBQyxHQUFHLENBQUM7Z0NBQ1AsUUFBUSxFQUFFLEdBQUc7Z0NBQ2IsSUFBSSxFQUFFLElBQUk7Z0NBQ1YsT0FBTyxFQUFFLE1BQU07NkJBQ2xCLENBQUMsQ0FBQTt3QkFDTixDQUFDO29CQUNMLENBQUM7Z0JBQ0wsQ0FBQztZQUNMLENBQUM7WUFDRCxNQUFNLENBQUMsTUFBTSxDQUFBO1FBQ2pCLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sSUFBSSxLQUFLLENBQUMsbUJBQWlCLFVBQVUsQ0FBQyxJQUFJLGtCQUFhLElBQUksQ0FBQyxJQUFNLENBQUMsQ0FBQTtRQUM3RSxDQUFDO0lBRUwsQ0FBQztJQUVPLHlCQUFRLEdBQWhCLFVBQWlCLEdBQVcsRUFBRSxNQUFXLEVBQUUsT0FBMEI7UUFBMUIsdUJBQTBCLEdBQTFCLFlBQTBCO1FBQ2pFLElBQU0sYUFBYSxHQUFHLElBQUksOEJBQWEsRUFBRSxDQUFBO1FBQ3pDLElBQU0sVUFBVSxHQUF5QixJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBeUIsQ0FBQTtRQUVqRixFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxZQUFZLFFBQVEsSUFBSSxVQUFVLENBQUMsSUFBSSxZQUFZLE1BQU0sSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUcsTUFBTSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFDeEUsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osTUFBTSxJQUFJLEtBQUssQ0FBQyxtQkFBaUIsVUFBVSxDQUFDLElBQUksa0JBQWEsSUFBSSxDQUFDLElBQU0sQ0FBQyxDQUFBO1FBQzdFLENBQUM7SUFDTCxDQUFDO0lBRUQseUJBQVEsR0FBUixVQUFTLE1BQVcsRUFBRSxHQUFnQyxFQUFFLE9BQTJCO1FBQy9FLEVBQUUsQ0FBQyxDQUFDLE9BQU8sT0FBTyxLQUFLLFdBQVcsSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwRCxPQUFPLEdBQUcsR0FBRyxDQUFBO1FBQ2pCLENBQUM7UUFDRCxPQUFPLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLElBQUksRUFBRSxFQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQTtRQUUxRCxFQUFFLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzFCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNoQixJQUFNLFlBQVksR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUE7Z0JBQzFFLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQTtZQUM3QyxDQUFDO1lBRUQsSUFBTSxNQUFNLEdBQUcsSUFBSSxxREFBd0IsRUFBRSxDQUFBO1lBRTdDLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBRyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dCQUMxQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxLQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2xDLE1BQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsS0FBRyxDQUFDLEVBQUUsS0FBRyxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFBO2dCQUNuRSxDQUFDO1lBQ0wsQ0FBQztZQUVELE1BQU0sQ0FBQyxNQUFNLENBQUE7UUFDakIsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ2hCLElBQU0sWUFBWSxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsbUJBQW1CLENBQUMsQ0FBQTtnQkFDMUUsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQTtZQUNyRCxDQUFDO1lBRUQsSUFBTSxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQTtZQUVsRSxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxHQUFHLElBQUksR0FBRyxNQUFNLENBQUE7UUFDM0MsQ0FBQztJQUNMLENBQUM7SUFFRCxzQkFBSyxHQUFMLFVBQU0sTUFBVyxFQUFFLE9BQTBCO1FBQTFCLHVCQUEwQixHQUExQixZQUEwQjtRQUN6QyxFQUFFLENBQUMsQ0FBQyxPQUFPLE1BQU0sS0FBSyxXQUFXLElBQUksTUFBTSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbkQsTUFBTSxDQUFDLE1BQU0sQ0FBQTtRQUNqQixDQUFDO1FBQ0QsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUE7UUFDL0MsSUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLE1BQU0sR0FBRyxNQUFNLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQTtRQUU1RCxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUMxQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ2xDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUE7WUFDckQsQ0FBQztRQUNMLENBQUM7UUFFRCxNQUFNLENBQUMsTUFBTSxDQUFBO0lBQ2pCLENBQUM7SUFFRCx1QkFBTSxHQUFOLFVBQU8sTUFBYztRQUNqQixNQUFNLENBQUMsSUFBSSxDQUFBO0lBQ2YsQ0FBQztJQUVELDhCQUFhLEdBQWIsVUFBYyxHQUFTLEVBQUUsTUFBWSxFQUFFLE9BQTJCO1FBQzlELEVBQUUsQ0FBQyxDQUFDLE9BQU8sR0FBRyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDMUIsT0FBTyxHQUFHLE9BQU8sT0FBTyxLQUFLLFFBQVEsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQTtZQUN4RixNQUFNLENBQUMsOEJBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFDcEYsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osT0FBTyxHQUFHLE1BQU0sQ0FBQTtZQUNoQixNQUFNLEdBQUcsR0FBRyxDQUFBO1lBQ1osT0FBTyxHQUFHLE9BQU8sT0FBTyxLQUFLLFFBQVEsR0FBRyxDQUFDLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQTtZQUN4RixJQUFNLFVBQVUsR0FBUSxFQUFFLENBQUE7WUFDMUIsR0FBRyxDQUFDLENBQUMsSUFBSSxLQUFHLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQzFCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLEtBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDbEMsSUFBTSxhQUFhLEdBQUcsRUFBRSxDQUFBO29CQUN4QixDQUFDLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSw4QkFBYSxDQUFDLG1CQUFtQixDQUFDLEtBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUcsQ0FBQyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFBO29CQUNsRyxVQUFVLENBQUMsS0FBRyxDQUFDLEdBQUcsYUFBYSxDQUFBO2dCQUNuQyxDQUFDO1lBQ0wsQ0FBQztZQUNELE1BQU0sQ0FBQyxVQUFVLENBQUE7UUFDckIsQ0FBQztJQUNMLENBQUM7SUFoSk0scUJBQWMsR0FBc0I7UUFDdkMsSUFBSSxFQUFFLFFBQVE7UUFDZCxLQUFLLEVBQUUsS0FBSztRQUNaLE1BQU0sRUFBRSxLQUFLO1FBQ2IsT0FBTyxFQUFFLEVBQUU7S0FDZCxDQUFBO0lBRU0sMEJBQW1CLEdBQWlCO1FBQ3ZDLE1BQU0sRUFBRSxLQUFLO1FBQ2IsV0FBVyxFQUFFLElBQUk7UUFDakIsa0JBQWtCLEVBQUUsSUFBSTtRQUN4QixrQkFBa0IsRUFBRSxJQUFJO1FBQ3hCLFFBQVEsRUFBRSxPQUFPO1FBQ2pCLE1BQU0sRUFBRSxLQUFLO1FBQ2IsV0FBVyxFQUFFLElBQUk7UUFDakIsYUFBYSxFQUFFLElBQUk7S0FDdEIsQ0FBQTtJQUVNLFlBQUssR0FBRztRQUNYLEtBQUssRUFBRSxzSUFBc0k7S0FDaEosQ0FBQTtJQTZITCxhQUFDO0FBQUQsQ0FySkEsQUFxSkMsSUFBQTtBQXJKWSxjQUFNLFNBcUpsQixDQUFBIiwiZmlsZSI6InNjaGVtYS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJ1xuXG5pbXBvcnQge1xuICAgIFZhbGlkYXRpb25PcHRpb25zLFxuICAgIFZhbGlkYXRpb25SZXN1bHQsXG4gICAgVmFsaWRhdGlvbkRlZmluaXRpb24sXG4gICAgQ2xlYW5PcHRpb25zXG59IGZyb20gJy4vaW50ZXJmYWNlcydcbmltcG9ydCB7IENvbXBvc2VkVmFsaWRhdGlvblJlc3VsdH0gZnJvbSAnLi9jb21wb3NlZC12YWxpZGF0aW9uLXJlc3VsdCdcbmltcG9ydCB7IFJvb3RWYWxpZGF0b3IgfSBmcm9tICcuL3ZhbGlkYXRvcnMvcm9vdC12YWxpZGF0b3InXG5cbmV4cG9ydCBjbGFzcyBTY2hlbWEge1xuXG4gICAgcmVhZG9ubHkgbmFtZTogU3RyaW5nXG5cbiAgICBzdGF0aWMgRGVmYXVsdE9wdGlvbnM6IFZhbGlkYXRpb25PcHRpb25zID0ge1xuICAgICAgICBuYW1lOiAnU2NoZW1hJyxcbiAgICAgICAgY2xlYW46IGZhbHNlLFxuICAgICAgICBzdHJpY3Q6IGZhbHNlLFxuICAgICAgICBjb250ZXh0OiB7fVxuICAgIH1cblxuICAgIHN0YXRpYyBEZWZhdWx0Q2xlYW5PcHRpb25zOiBDbGVhbk9wdGlvbnMgPSB7XG4gICAgICAgIG11dGF0ZTogZmFsc2UsXG4gICAgICAgIHRyaW1TdHJpbmdzOiB0cnVlLFxuICAgICAgICByZW1vdmVFbXB0eVN0cmluZ3M6IHRydWUsXG4gICAgICAgIHJlbW92ZUVtcHR5T2JqZWN0czogdHJ1ZSxcbiAgICAgICAgcm91bmRpbmc6ICdyb3VuZCcsXG4gICAgICAgIGZpbHRlcjogZmFsc2UsXG4gICAgICAgIGF1dG9Db252ZXJ0OiB0cnVlLFxuICAgICAgICBnZXRBdXRvVmFsdWVzOiB0cnVlXG4gICAgfVxuXG4gICAgc3RhdGljIFJlZ0V4ID0ge1xuICAgICAgICBFbWFpbDogL15bYS16QS1aMC05LiEjJCUmJyorLz0/Xl9ge3x9fi1dK0BbYS16QS1aMC05XSg/OlthLXpBLVowLTktXXswLDYxfVthLXpBLVowLTldKT8oPzpcXC5bYS16QS1aMC05XSg/OlthLXpBLVowLTktXXswLDYxfVthLXpBLVowLTldKT8pKiQvXG4gICAgfVxuXG4gICAgY29uc3RydWN0b3IocHJvdGVjdGVkIHNjaGVtYTogYW55LCBwcml2YXRlIG9wdGlvbnM6IFZhbGlkYXRpb25PcHRpb25zID0ge30pIHtcbiAgICAgICAgXy5kZWZhdWx0cyhvcHRpb25zLCBTY2hlbWEuRGVmYXVsdE9wdGlvbnMpXG4gICAgfVxuXG4gICAgcHJpdmF0ZSB2YWxpZGF0ZUtleSh2YWx1ZTogYW55LCBrZXk6IHN0cmluZywgb2JqZWN0OiBhbnksIG9wdGlvbnM6IFZhbGlkYXRpb25PcHRpb25zKTogVmFsaWRhdGlvblJlc3VsdCB7XG4gICAgICAgIGNvbnN0IHJvb3RWYWxpZGF0b3IgPSBuZXcgUm9vdFZhbGlkYXRvcigpXG4gICAgICAgIGNvbnN0IGRlZmluaXRpb246IFZhbGlkYXRpb25EZWZpbml0aW9uID0gdGhpcy5zY2hlbWFba2V5XSBhcyBWYWxpZGF0aW9uRGVmaW5pdGlvblxuXG4gICAgICAgIGlmIChkZWZpbml0aW9uLnR5cGUgaW5zdGFuY2VvZiBGdW5jdGlvbiB8fCBkZWZpbml0aW9uLnR5cGUgaW5zdGFuY2VvZiBTY2hlbWEgfHwgXy5pc09iamVjdChkZWZpbml0aW9uLnR5cGUpKSB7XG4gICAgICAgICAgICBjb25zdCByZXN1bHQgPSByb290VmFsaWRhdG9yLnZhbGlkYXRlKGtleSwgZGVmaW5pdGlvbiwgdmFsdWUsIG9wdGlvbnMpIGFzIENvbXBvc2VkVmFsaWRhdGlvblJlc3VsdFxuICAgICAgICAgICAgaWYgKHR5cGVvZiBkZWZpbml0aW9uLmN1c3RvbSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgIGxldCBjdXN0b20gPSBkZWZpbml0aW9uLmN1c3RvbSh2YWx1ZSwgb2JqZWN0LCBvcHRpb25zLmNvbnRleHQpXG4gICAgICAgICAgICAgICAgaWYgKGN1c3RvbSkge1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQuYW5kKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHByb3BlcnR5OiBrZXksXG4gICAgICAgICAgICAgICAgICAgICAgICBydWxlOiAnY3VzdG9tJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IGN1c3RvbVxuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGRlZmluaXRpb24uY3VzdG9tID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgICAgICAgIGZvciAobGV0IHJ1bGUgaW4gZGVmaW5pdGlvbi5jdXN0b20pIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRlZmluaXRpb24uY3VzdG9tLmhhc093blByb3BlcnR5KHJ1bGUpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgY3VzdG9tID0gZGVmaW5pdGlvbi5jdXN0b21bcnVsZV0odmFsdWUsIG9iamVjdCwgb3B0aW9ucy5jb250ZXh0KVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGN1c3RvbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdC5hbmQoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9wZXJ0eToga2V5LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBydWxlOiBydWxlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBjdXN0b21cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBJbnZhbGlkIHR5cGUgJyR7ZGVmaW5pdGlvbi50eXBlfScgdXNlZCBpbiAke3RoaXMubmFtZX1gKVxuICAgICAgICB9XG5cbiAgICB9XG5cbiAgICBwcml2YXRlIGNsZWFuS2V5KGtleTogc3RyaW5nLCBvYmplY3Q6IGFueSwgb3B0aW9uczogQ2xlYW5PcHRpb25zID0ge30pIHtcbiAgICAgICAgY29uc3Qgcm9vdFZhbGlkYXRvciA9IG5ldyBSb290VmFsaWRhdG9yKClcbiAgICAgICAgY29uc3QgZGVmaW5pdGlvbjogVmFsaWRhdGlvbkRlZmluaXRpb24gPSB0aGlzLnNjaGVtYVtrZXldIGFzIFZhbGlkYXRpb25EZWZpbml0aW9uXG5cbiAgICAgICAgaWYgKGRlZmluaXRpb24udHlwZSBpbnN0YW5jZW9mIEZ1bmN0aW9uIHx8IGRlZmluaXRpb24udHlwZSBpbnN0YW5jZW9mIFNjaGVtYSB8fCBfLmlzT2JqZWN0KGRlZmluaXRpb24udHlwZSkpIHtcbiAgICAgICAgICAgIHJldHVybiByb290VmFsaWRhdG9yLmNsZWFuKGRlZmluaXRpb24sIG9iamVjdFtrZXldLCBvcHRpb25zLCBvYmplY3QpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYEludmFsaWQgdHlwZSAnJHtkZWZpbml0aW9uLnR5cGV9JyB1c2VkIGluICR7dGhpcy5uYW1lfWApXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICB2YWxpZGF0ZShvYmplY3Q6IGFueSwga2V5Pzogc3RyaW5nIHwgVmFsaWRhdGlvbk9wdGlvbnMsIG9wdGlvbnM/OiBWYWxpZGF0aW9uT3B0aW9ucyk6IFZhbGlkYXRpb25SZXN1bHQgfCBudWxsIHtcbiAgICAgICAgaWYgKHR5cGVvZiBvcHRpb25zID09PSAndW5kZWZpbmVkJyAmJiBfLmlzT2JqZWN0KGtleSkpIHtcbiAgICAgICAgICAgIG9wdGlvbnMgPSBrZXlcbiAgICAgICAgfVxuICAgICAgICBvcHRpb25zID0gXy5kZWZhdWx0cyhvcHRpb25zIHx8IHt9LCBTY2hlbWEuRGVmYXVsdE9wdGlvbnMpXG5cbiAgICAgICAgaWYgKHR5cGVvZiBrZXkgIT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICBpZiAob3B0aW9ucy5jbGVhbikge1xuICAgICAgICAgICAgICAgIGNvbnN0IGNsZWFuT3B0aW9ucyA9IF8uZGVmYXVsdHMob3B0aW9ucy5jbGVhbiwgU2NoZW1hLkRlZmF1bHRDbGVhbk9wdGlvbnMpXG4gICAgICAgICAgICAgICAgb2JqZWN0ID0gdGhpcy5jbGVhbihvYmplY3QsIGNsZWFuT3B0aW9ucylcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29uc3QgcmVzdWx0ID0gbmV3IENvbXBvc2VkVmFsaWRhdGlvblJlc3VsdCgpXG5cbiAgICAgICAgICAgIGZvciAobGV0IGtleSBpbiB0aGlzLnNjaGVtYSkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnNjaGVtYS5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdC5hbmQodGhpcy52YWxpZGF0ZUtleShvYmplY3Rba2V5XSwga2V5LCBvYmplY3QsIG9wdGlvbnMpKVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKG9wdGlvbnMuY2xlYW4pIHtcbiAgICAgICAgICAgICAgICBjb25zdCBjbGVhbk9wdGlvbnMgPSBfLmRlZmF1bHRzKG9wdGlvbnMuY2xlYW4sIFNjaGVtYS5EZWZhdWx0Q2xlYW5PcHRpb25zKVxuICAgICAgICAgICAgICAgIG9iamVjdCA9IHRoaXMuY2xlYW5LZXkoa2V5LCBvYmplY3QsIGNsZWFuT3B0aW9ucylcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy52YWxpZGF0ZUtleShvYmplY3Rba2V5XSwga2V5LCBvYmplY3QsIG9wdGlvbnMpXG5cbiAgICAgICAgICAgIHJldHVybiByZXN1bHQuaXNWYWxpZCgpID8gbnVsbCA6IHJlc3VsdFxuICAgICAgICB9XG4gICAgfVxuXG4gICAgY2xlYW4ob2JqZWN0OiBhbnksIG9wdGlvbnM6IENsZWFuT3B0aW9ucyA9IHt9KTogYW55IHtcbiAgICAgICAgaWYgKHR5cGVvZiBvYmplY3QgPT09ICd1bmRlZmluZWQnIHx8IG9iamVjdCA9PT0gbnVsbCkge1xuICAgICAgICAgICAgcmV0dXJuIG9iamVjdFxuICAgICAgICB9XG4gICAgICAgIF8uZGVmYXVsdHMob3B0aW9ucywgU2NoZW1hLkRlZmF1bHRDbGVhbk9wdGlvbnMpXG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IG9wdGlvbnMubXV0YXRlID8gb2JqZWN0IDogXy5jbG9uZURlZXAob2JqZWN0KVxuXG4gICAgICAgIGZvciAobGV0IGtleSBpbiB0aGlzLnNjaGVtYSkge1xuICAgICAgICAgICAgaWYgKHRoaXMuc2NoZW1hLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgICAgICByZXN1bHRba2V5XSA9IHRoaXMuY2xlYW5LZXkoa2V5LCBvYmplY3QsIG9wdGlvbnMpXG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgfVxuXG4gICAgZXh0ZW5kKHNjaGVtYTogU2NoZW1hKTogU2NoZW1hIHtcbiAgICAgICAgcmV0dXJuIHRoaXNcbiAgICB9XG5cbiAgICBnZXRWYWxpZGF0b3JzKGtleT86IGFueSwgb2JqZWN0PzogYW55LCBvcHRpb25zPzogVmFsaWRhdGlvbk9wdGlvbnMpOiBhbnkge1xuICAgICAgICBpZiAodHlwZW9mIGtleSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgICAgIG9wdGlvbnMgPSB0eXBlb2Ygb3B0aW9ucyA9PT0gJ29iamVjdCcgPyBfLmRlZmF1bHRzKG9wdGlvbnMsIHRoaXMub3B0aW9ucykgOiB0aGlzLm9wdGlvbnNcbiAgICAgICAgICAgIHJldHVybiBSb290VmFsaWRhdG9yLmdldFZhbGlkYXRvcnNGb3JLZXkoa2V5LCB0aGlzLnNjaGVtYVtrZXldLCBvcHRpb25zLCBvYmplY3QpXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBvcHRpb25zID0gb2JqZWN0XG4gICAgICAgICAgICBvYmplY3QgPSBrZXlcbiAgICAgICAgICAgIG9wdGlvbnMgPSB0eXBlb2Ygb3B0aW9ucyA9PT0gJ29iamVjdCcgPyBfLmRlZmF1bHRzKG9wdGlvbnMsIHRoaXMub3B0aW9ucykgOiB0aGlzLm9wdGlvbnNcbiAgICAgICAgICAgIGNvbnN0IHZhbGlkYXRvcnM6IGFueSA9IHt9XG4gICAgICAgICAgICBmb3IgKGxldCBrZXkgaW4gdGhpcy5zY2hlbWEpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5zY2hlbWEuaGFzT3duUHJvcGVydHkoa2V5KSkge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCBrZXlWYWxpZGF0b3JzID0ge31cbiAgICAgICAgICAgICAgICAgICAgXy5hc3NpZ24oa2V5VmFsaWRhdG9ycywgUm9vdFZhbGlkYXRvci5nZXRWYWxpZGF0b3JzRm9yS2V5KGtleSwgdGhpcy5zY2hlbWFba2V5XSwgb3B0aW9ucywgb2JqZWN0KSlcbiAgICAgICAgICAgICAgICAgICAgdmFsaWRhdG9yc1trZXldID0ga2V5VmFsaWRhdG9yc1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiB2YWxpZGF0b3JzXG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=
