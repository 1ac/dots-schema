import * as _ from 'lodash'

import {
    Validator,
    ValidationDefinition,
    DefinitionType,
    ValidationResult,
    ValidationOptions,
    ValidationError,
    CleanOptions
} from './interfaces'
import { RootValidator } from './validators/root-validator'
import { Schema } from './schema'

export function cleaned(validator: any, key: string, definition: ValidationDefinition, options: ValidationOptions, defaultObject?: any, custom?: Function, rule?: string): Function {
    const rootValidator = new RootValidator()
    const defaultOptions = options
    const defaultCleanOptions = typeof options.clean === 'object' ? _.defaults({}, options.clean, Schema.DefaultCleanOptions) : _.defaults({}, Schema.DefaultCleanOptions)

    return (value: any, object?: any, options?: ValidationOptions) => {
        let cleanOptions = defaultCleanOptions
        options = _.assign({}, defaultOptions, options)

        object = typeof object !== 'undefined' ? object : defaultObject

        if (options.clean) {
            const cleanOptions = options.clean === 'object' ? _.assign({}, defaultCleanOptions, options.clean) : _.assign({}, defaultCleanOptions)
            value = rootValidator.clean(definition, value, cleanOptions, object)
        }

        return validator(value, key, definition, object, options, custom, rule)
    }
}
