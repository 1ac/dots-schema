"use strict";
var _ = require('lodash');
var composed_validation_result_1 = require('../composed-validation-result');
var cleaned_1 = require('../cleaned');
var BooleanValidator = (function () {
    function BooleanValidator() {
    }
    BooleanValidator.getValidatorsForKey = function (key, definition, options, object) {
        return {
            type: cleaned_1.cleaned(BooleanValidator.RULES.type, key, definition, options)
        };
    };
    BooleanValidator.prototype.validate = function (key, definition, value, options) {
        var result = new composed_validation_result_1.ComposedValidationResult();
        var rules = BooleanValidator.RULES;
        result.and(rules.type(value, key, definition));
        return result;
    };
    BooleanValidator.prototype.clean = function (definition, value, options, object) {
        if (!options.autoConvert || typeof value === 'undefined') {
            return value;
        }
        if (typeof value === 'string') {
            if (_.toLower(value) === 'false') {
                return false;
            }
        }
        else if (value) {
            return true;
        }
        else {
            return false;
        }
    };
    BooleanValidator.RULES = {
        type: function (value, key, definition) {
            if ((typeof value !== 'undefined' && value !== null) && typeof value !== 'boolean') {
                return {
                    property: key,
                    rule: 'type',
                    message: "Property " + key + " must be of type Boolean"
                };
            }
            return null;
        }
    };
    return BooleanValidator;
}());
exports.BooleanValidator = BooleanValidator;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZhbGlkYXRvcnMvYm9vbGVhbi12YWxpZGF0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLElBQVksQ0FBQyxXQUFNLFFBRW5CLENBQUMsQ0FGMEI7QUFTM0IsMkNBQXlDLCtCQUN6QyxDQUFDLENBRHVFO0FBQ3hFLHdCQUF3QixZQUV4QixDQUFDLENBRm1DO0FBRXBDO0lBQUE7SUE2Q0EsQ0FBQztJQTlCaUIsb0NBQW1CLEdBQWpDLFVBQWtDLEdBQVcsRUFBRSxVQUFnQyxFQUFFLE9BQTBCLEVBQUUsTUFBWTtRQUNySCxNQUFNLENBQUM7WUFDSCxJQUFJLEVBQUUsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsT0FBTyxDQUFDO1NBQ3ZFLENBQUE7SUFDTCxDQUFDO0lBRUQsbUNBQVEsR0FBUixVQUFTLEdBQVcsRUFBRSxVQUFnQyxFQUFFLEtBQVUsRUFBRSxPQUEwQjtRQUMxRixJQUFNLE1BQU0sR0FBRyxJQUFJLHFEQUF3QixFQUFFLENBQUE7UUFDN0MsSUFBTSxLQUFLLEdBQUcsZ0JBQWdCLENBQUMsS0FBSyxDQUFBO1FBRXBDLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUE7UUFFOUMsTUFBTSxDQUFDLE1BQU0sQ0FBQTtJQUNqQixDQUFDO0lBRUQsZ0NBQUssR0FBTCxVQUFNLFVBQWdDLEVBQUUsS0FBVSxFQUFFLE9BQXFCLEVBQUUsTUFBVztRQUNsRixFQUFFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLElBQUksT0FBTyxLQUFLLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQztZQUN2RCxNQUFNLENBQUMsS0FBSyxDQUFBO1FBQ2hCLENBQUM7UUFDRCxFQUFFLENBQUMsQ0FBQyxPQUFPLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzVCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQztnQkFDL0IsTUFBTSxDQUFDLEtBQUssQ0FBQTtZQUNoQixDQUFDO1FBQ0wsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ2YsTUFBTSxDQUFDLElBQUksQ0FBQTtRQUNmLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLE1BQU0sQ0FBQyxLQUFLLENBQUE7UUFDaEIsQ0FBQztJQUNMLENBQUM7SUF6Q2Esc0JBQUssR0FBRztRQUNsQixJQUFJLEVBQUUsVUFBQyxLQUFVLEVBQUUsR0FBVyxFQUFFLFVBQWdDO1lBQzVELEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxLQUFLLEtBQUssV0FBVyxJQUFJLEtBQUssS0FBSyxJQUFJLENBQUMsSUFBSSxPQUFPLEtBQUssS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNqRixNQUFNLENBQUM7b0JBQ0gsUUFBUSxFQUFFLEdBQUc7b0JBQ2IsSUFBSSxFQUFFLE1BQU07b0JBQ1osT0FBTyxFQUFFLGNBQVksR0FBRyw2QkFBMEI7aUJBQ3JELENBQUE7WUFDTCxDQUFDO1lBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQTtRQUNmLENBQUM7S0FDSixDQUFBO0lBZ0NMLHVCQUFDO0FBQUQsQ0E3Q0EsQUE2Q0MsSUFBQTtBQTdDWSx3QkFBZ0IsbUJBNkM1QixDQUFBIiwiZmlsZSI6InZhbGlkYXRvcnMvYm9vbGVhbi12YWxpZGF0b3IuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCdcblxuaW1wb3J0IHtcbiAgICBWYWxpZGF0b3IsXG4gICAgVmFsaWRhdGlvbkRlZmluaXRpb24sXG4gICAgVmFsaWRhdGlvblJlc3VsdCxcbiAgICBWYWxpZGF0aW9uT3B0aW9ucyxcbiAgICBDbGVhbk9wdGlvbnNcbn0gIGZyb20gJy4uL2ludGVyZmFjZXMnXG5pbXBvcnQgeyBDb21wb3NlZFZhbGlkYXRpb25SZXN1bHQgfSBmcm9tICcuLi9jb21wb3NlZC12YWxpZGF0aW9uLXJlc3VsdCdcbmltcG9ydCB7IGNsZWFuZWQgfSBmcm9tICcuLi9jbGVhbmVkJ1xuXG5leHBvcnQgY2xhc3MgQm9vbGVhblZhbGlkYXRvciBpbXBsZW1lbnRzIFZhbGlkYXRvciB7XG5cbiAgICBwdWJsaWMgc3RhdGljIFJVTEVTID0ge1xuICAgICAgICB0eXBlOiAodmFsdWU6IGFueSwga2V5OiBzdHJpbmcsIGRlZmluaXRpb246IFZhbGlkYXRpb25EZWZpbml0aW9uKSA9PiB7XG4gICAgICAgICAgICBpZiAoKHR5cGVvZiB2YWx1ZSAhPT0gJ3VuZGVmaW5lZCcgJiYgdmFsdWUgIT09IG51bGwpICYmIHR5cGVvZiB2YWx1ZSAhPT0gJ2Jvb2xlYW4nKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgICAgICAgcHJvcGVydHk6IGtleSxcbiAgICAgICAgICAgICAgICAgICAgcnVsZTogJ3R5cGUnLFxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBgUHJvcGVydHkgJHtrZXl9IG11c3QgYmUgb2YgdHlwZSBCb29sZWFuYFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBudWxsXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgc3RhdGljIGdldFZhbGlkYXRvcnNGb3JLZXkoa2V5OiBzdHJpbmcsIGRlZmluaXRpb246IFZhbGlkYXRpb25EZWZpbml0aW9uLCBvcHRpb25zOiBWYWxpZGF0aW9uT3B0aW9ucywgb2JqZWN0PzogYW55KSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICB0eXBlOiBjbGVhbmVkKEJvb2xlYW5WYWxpZGF0b3IuUlVMRVMudHlwZSwga2V5LCBkZWZpbml0aW9uLCBvcHRpb25zKVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgdmFsaWRhdGUoa2V5OiBzdHJpbmcsIGRlZmluaXRpb246IFZhbGlkYXRpb25EZWZpbml0aW9uLCB2YWx1ZTogYW55LCBvcHRpb25zOiBWYWxpZGF0aW9uT3B0aW9ucyk6IFZhbGlkYXRpb25SZXN1bHQge1xuICAgICAgICBjb25zdCByZXN1bHQgPSBuZXcgQ29tcG9zZWRWYWxpZGF0aW9uUmVzdWx0KClcbiAgICAgICAgY29uc3QgcnVsZXMgPSBCb29sZWFuVmFsaWRhdG9yLlJVTEVTXG5cbiAgICAgICAgcmVzdWx0LmFuZChydWxlcy50eXBlKHZhbHVlLCBrZXksIGRlZmluaXRpb24pKVxuXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICB9XG5cbiAgICBjbGVhbihkZWZpbml0aW9uOiBWYWxpZGF0aW9uRGVmaW5pdGlvbiwgdmFsdWU6IGFueSwgb3B0aW9uczogQ2xlYW5PcHRpb25zLCBvYmplY3Q6IGFueSk6IGFueSB7XG4gICAgICAgIGlmICghb3B0aW9ucy5hdXRvQ29udmVydCB8fCB0eXBlb2YgdmFsdWUgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICByZXR1cm4gdmFsdWVcbiAgICAgICAgfVxuICAgICAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgaWYgKF8udG9Mb3dlcih2YWx1ZSkgPT09ICdmYWxzZScpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2VcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIGlmICh2YWx1ZSkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZVxuICAgICAgICB9XG4gICAgfVxuXG59XG4iXX0=
