"use strict";
var _ = require('lodash');
var composed_validation_result_1 = require('../composed-validation-result');
var string_validator_1 = require('./string-validator');
var number_validator_1 = require('./number-validator');
var date_validator_1 = require('./date-validator');
var object_validator_1 = require('./object-validator');
var schema_validator_1 = require('./schema-validator');
var boolean_validator_1 = require('./boolean-validator');
var schema_1 = require('../schema');
var cleaned_1 = require('../cleaned');
var RootValidator = (function () {
    function RootValidator() {
    }
    RootValidator.getValidatorsForKey = function (key, definition, options, object) {
        var validators = {};
        if (!definition.optional) {
            validators.required = cleaned_1.cleaned(RootValidator.RULES.required, key, definition, options);
        }
        if (definition.allowedValues) {
            validators.allowedValues = cleaned_1.cleaned(RootValidator.RULES.allowedValues, key, definition, options);
        }
        if (definition.array) {
            validators.isArray = cleaned_1.cleaned(RootValidator.RULES.isArray, key, definition, options);
        }
        if (definition.minCount) {
            validators.minCount = cleaned_1.cleaned(RootValidator.RULES.minCount, key, definition, options);
        }
        if (definition.maxCount) {
            validators.maxCount = cleaned_1.cleaned(RootValidator.RULES.maxCount, key, definition, options);
        }
        if (definition.custom) {
            if (typeof definition.custom === 'function') {
                validators.custom = cleaned_1.cleaned(RootValidator.RULES.custom, key, definition, options, object, definition.custom, 'custom');
            }
            else if (typeof definition.custom === 'object') {
                for (var rule in definition.custom) {
                    if (definition.custom.hasOwnProperty(rule)) {
                        validators[rule] = cleaned_1.cleaned(RootValidator.RULES.custom, key, definition, options, object, definition.custom[rule], rule);
                    }
                }
            }
        }
        switch (definition.type) {
            case Boolean:
                _.assign(validators, boolean_validator_1.BooleanValidator.getValidatorsForKey(key, definition, options, object));
                break;
            case Date:
                _.assign(validators, date_validator_1.DateValidator.getValidatorsForKey(key, definition, options, object));
                break;
            case Number:
                _.assign(validators, number_validator_1.NumberValidator.getValidatorsForKey(key, definition, options, object));
                break;
            case Object:
                _.assign(validators, object_validator_1.ObjectValidator.getValidatorsForKey(key, definition, options, object));
                break;
            case String:
                _.assign(validators, string_validator_1.StringValidator.getValidatorsForKey(key, definition, options, object));
                break;
            default:
                if (definition.type instanceof schema_1.Schema) {
                    _.assign(validators, schema_validator_1.SchemaValidator.getValidatorsForKey(key, definition, options, object));
                }
                else {
                    throw new Error("Unkown type " + definition.type + " used in schema");
                }
        }
        return validators;
    };
    RootValidator.getValidator = function (type) {
        switch (type) {
            case String:
                return string_validator_1.StringValidator;
            case Number:
                return number_validator_1.NumberValidator;
            case Date:
                return date_validator_1.DateValidator;
            case Object:
                return object_validator_1.ObjectValidator;
            case Boolean:
                return boolean_validator_1.BooleanValidator;
            default:
                if (type instanceof schema_1.Schema) {
                    return schema_validator_1.SchemaValidator;
                }
                else {
                    throw new Error("Unkown type " + type + " used in schema");
                }
        }
    };
    RootValidator.prototype.getValidator = function (type) {
        switch (type) {
            case String:
                return new string_validator_1.StringValidator();
            case Number:
                return new number_validator_1.NumberValidator();
            case Date:
                return new date_validator_1.DateValidator();
            case Object:
                return new object_validator_1.ObjectValidator();
            case Boolean:
                return new boolean_validator_1.BooleanValidator();
            default:
                if (type instanceof schema_1.Schema) {
                    return new schema_validator_1.SchemaValidator();
                }
                else {
                    throw new Error("Unkown type " + type + " used in schema");
                }
        }
    };
    RootValidator.prototype.validateType = function (key, definition, value, options) {
        var result = new composed_validation_result_1.ComposedValidationResult();
        var type = definition.type;
        var rules = RootValidator.RULES;
        var validator = this.getValidator(type);
        if (definition.array) {
            result.and(rules.isArray(value, key, definition));
            if (result.isValid()) {
                result.and(rules.minCount(value, key, definition));
                result.and(rules.maxCount(value, key, definition));
                // use classic for loop here because we need the index
                for (var index = 0; index < value.length; index++) {
                    result.and(validator.validate(key + "." + index, definition, value[index], options));
                }
            }
        }
        else {
            result.and(validator.validate(key, definition, value, options));
        }
        return result;
    };
    RootValidator.prototype.validate = function (key, definition, value, options) {
        var result = new composed_validation_result_1.ComposedValidationResult();
        var rules = RootValidator.RULES;
        result.and(rules.required(value, key, definition));
        if (!result.isValid() || (typeof value === 'undefined' || value == null)) {
            return result;
        }
        result.and(rules.allowedValues(value, key, definition));
        if (!result.isValid()) {
            return result;
        }
        var types = Array.isArray(definition.type) ? definition.type : [definition.type];
        for (var _i = 0, types_1 = types; _i < types_1.length; _i++) {
            var type = types_1[_i];
            var singleDefinition = { type: type };
            _.defaults(singleDefinition, definition);
            result.or(this.validateType(key, singleDefinition, value, options));
            if (result.isValid()) {
                break;
            }
        }
        return result;
    };
    RootValidator.prototype.clean = function (definition, value, options, object) {
        var result = value;
        if (options.removeEmptyStrings && typeof result === 'string' && value.trim().length === 0) {
            if (definition.removeEmpty !== false) {
                result = null;
            }
        }
        else if (options.removeEmptyObjects && typeof result === 'object' && _.isEmpty(result) && !_.isDate(result)) {
            if (definition.removeEmpty !== false) {
                result = null;
            }
        }
        var types = Array.isArray(definition.type) ? definition.type : [definition.type];
        if (typeof result === 'undefined' || result == null) {
            if (typeof definition.defaultValue !== 'undefined') {
                result = _.cloneDeep(definition.defaultValue);
            }
        }
        if (options.getAutoValues && typeof definition.autoValue === 'function') {
            result = definition.autoValue(result, object);
        }
        for (var _i = 0, _a = _.reverse(types); _i < _a.length; _i++) {
            var type = _a[_i];
            result = this.getValidator(type).clean(definition, result, options, object);
        }
        return result;
    };
    RootValidator.RULES = {
        isArray: function (value, key, definition) {
            if ((typeof value !== 'undefined' && value !== null) && !Array.isArray(value)) {
                return {
                    property: key,
                    rule: 'type',
                    message: "Property " + key + " expected to be an array of type " + definition.type.name
                };
            }
            return null;
        },
        minCount: function (value, key, definition) {
            if ((typeof value !== 'undefined' && value !== null) && (typeof definition.minCount === 'number') && value.length < definition.minCount) {
                return {
                    property: key,
                    rule: 'minCount',
                    message: "Property " + key + " expected to be an array of type " + definition.type.name + " with at least " + definition.minCount + " elements"
                };
            }
            return null;
        },
        maxCount: function (value, key, definition) {
            if ((typeof value !== 'undefined' && value !== null) && (typeof definition.maxCount === 'number') && value.length > definition.maxCount) {
                return {
                    property: key,
                    rule: 'maxCount',
                    message: "Property " + key + " expected to be an array of type " + definition.type.name + " with at max " + definition.maxCount + " elements"
                };
            }
            return null;
        },
        required: function (value, key, definition) {
            if (!definition.optional && (typeof value === 'undefined' || value == null)) {
                return {
                    property: key,
                    rule: 'required',
                    message: "Missing value for property " + key
                };
            }
            return null;
        },
        allowedValues: function (value, key, definition) {
            if ((typeof value !== 'undefined' && value !== null) && typeof definition.allowedValues !== 'undefined') {
                if (!(value in definition.allowedValues)) {
                    return {
                        property: key,
                        rule: 'allowedValues',
                        message: "Value of " + key + " is not in allowedValues"
                    };
                }
            }
            return null;
        },
        custom: function (value, key, defintion, object, options, custom, rule) {
            var error = custom(value, object, options.context);
            if (typeof error === 'string') {
                return {
                    property: key,
                    rule: rule,
                    message: error
                };
            }
            return null;
        }
    };
    return RootValidator;
}());
exports.RootValidator = RootValidator;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInZhbGlkYXRvcnMvcm9vdC12YWxpZGF0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLElBQVksQ0FBQyxXQUFNLFFBRW5CLENBQUMsQ0FGMEI7QUFXM0IsMkNBQXlDLCtCQUN6QyxDQUFDLENBRHVFO0FBQ3hFLGlDQUFnQyxvQkFDaEMsQ0FBQyxDQURtRDtBQUNwRCxpQ0FBZ0Msb0JBQ2hDLENBQUMsQ0FEbUQ7QUFDcEQsK0JBQThCLGtCQUM5QixDQUFDLENBRCtDO0FBQ2hELGlDQUFnQyxvQkFDaEMsQ0FBQyxDQURtRDtBQUNwRCxpQ0FBZ0Msb0JBQ2hDLENBQUMsQ0FEbUQ7QUFDcEQsa0NBQWlDLHFCQUNqQyxDQUFDLENBRHFEO0FBQ3RELHVCQUF1QixXQUN2QixDQUFDLENBRGlDO0FBQ2xDLHdCQUF3QixZQUV4QixDQUFDLENBRm1DO0FBRXBDO0lBQUE7SUFpUUEsQ0FBQztJQTNMaUIsaUNBQW1CLEdBQWpDLFVBQWtDLEdBQVcsRUFBRSxVQUFnQyxFQUFFLE9BQTBCLEVBQUUsTUFBWTtRQUNySCxJQUFNLFVBQVUsR0FBUSxFQUFFLENBQUE7UUFFMUIsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN2QixVQUFVLENBQUMsUUFBUSxHQUFHLGlCQUFPLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQTtRQUN6RixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDM0IsVUFBVSxDQUFDLGFBQWEsR0FBRyxpQkFBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUE7UUFDbkcsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ25CLFVBQVUsQ0FBQyxPQUFPLEdBQUcsaUJBQU8sQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFBO1FBQ3ZGLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUN0QixVQUFVLENBQUMsUUFBUSxHQUFHLGlCQUFPLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxPQUFPLENBQUMsQ0FBQTtRQUN6RixDQUFDO1FBRUQsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDdEIsVUFBVSxDQUFDLFFBQVEsR0FBRyxpQkFBTyxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUE7UUFDekYsQ0FBQztRQUVELEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBRSxDQUFDO1lBQ3JCLEVBQUUsQ0FBQyxDQUFDLE9BQU8sVUFBVSxDQUFDLE1BQU0sS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDO2dCQUMxQyxVQUFVLENBQUMsTUFBTSxHQUFHLGlCQUFPLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUE7WUFDMUgsQ0FBQztZQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLFVBQVUsQ0FBQyxNQUFNLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQztnQkFDL0MsR0FBRyxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ2pDLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDekMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLGlCQUFPLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUE7b0JBQzNILENBQUM7Z0JBQ0wsQ0FBQztZQUNMLENBQUM7UUFDTCxDQUFDO1FBRUQsTUFBTSxDQUFDLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDMUIsS0FBSyxPQUFPO2dCQUNSLENBQUMsQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLG9DQUFnQixDQUFDLG1CQUFtQixDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUE7Z0JBQzVGLEtBQUssQ0FBQTtZQUNULEtBQUssSUFBSTtnQkFDTCxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSw4QkFBYSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUE7Z0JBQ3pGLEtBQUssQ0FBQTtZQUNULEtBQUssTUFBTTtnQkFDUCxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxrQ0FBZSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUE7Z0JBQzNGLEtBQUssQ0FBQTtZQUNULEtBQUssTUFBTTtnQkFDUCxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxrQ0FBZSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUE7Z0JBQzNGLEtBQUssQ0FBQTtZQUNULEtBQUssTUFBTTtnQkFDUCxDQUFDLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxrQ0FBZSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUE7Z0JBQzNGLEtBQUssQ0FBQTtZQUNUO2dCQUNJLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLFlBQVksZUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDcEMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsa0NBQWUsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFBO2dCQUMvRixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLE1BQU0sSUFBSSxLQUFLLENBQUMsaUJBQWUsVUFBVSxDQUFDLElBQUksb0JBQWlCLENBQUMsQ0FBQTtnQkFDcEUsQ0FBQztRQUNMLENBQUM7UUFDRCxNQUFNLENBQUMsVUFBVSxDQUFBO0lBQ3JCLENBQUM7SUFFYSwwQkFBWSxHQUExQixVQUEyQixJQUFvQjtRQUMzQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ1gsS0FBSyxNQUFNO2dCQUNQLE1BQU0sQ0FBQyxrQ0FBZSxDQUFBO1lBQzFCLEtBQUssTUFBTTtnQkFDUCxNQUFNLENBQUMsa0NBQWUsQ0FBQTtZQUMxQixLQUFLLElBQUk7Z0JBQ0wsTUFBTSxDQUFDLDhCQUFhLENBQUE7WUFDeEIsS0FBSyxNQUFNO2dCQUNQLE1BQU0sQ0FBQyxrQ0FBZSxDQUFBO1lBQzFCLEtBQUssT0FBTztnQkFDUixNQUFNLENBQUMsb0NBQWdCLENBQUE7WUFDM0I7Z0JBQ0ksRUFBRSxDQUFDLENBQUMsSUFBSSxZQUFZLGVBQU0sQ0FBQyxDQUFDLENBQUM7b0JBQ3pCLE1BQU0sQ0FBQyxrQ0FBZSxDQUFBO2dCQUMxQixDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLE1BQU0sSUFBSSxLQUFLLENBQUMsaUJBQWUsSUFBSSxvQkFBaUIsQ0FBQyxDQUFBO2dCQUN6RCxDQUFDO1FBQ1QsQ0FBQztJQUNMLENBQUM7SUFFTyxvQ0FBWSxHQUFwQixVQUFxQixJQUFvQjtRQUNyQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ1gsS0FBSyxNQUFNO2dCQUNQLE1BQU0sQ0FBQyxJQUFJLGtDQUFlLEVBQUUsQ0FBQTtZQUNoQyxLQUFLLE1BQU07Z0JBQ1AsTUFBTSxDQUFDLElBQUksa0NBQWUsRUFBRSxDQUFBO1lBQ2hDLEtBQUssSUFBSTtnQkFDTCxNQUFNLENBQUMsSUFBSSw4QkFBYSxFQUFFLENBQUE7WUFDOUIsS0FBSyxNQUFNO2dCQUNQLE1BQU0sQ0FBQyxJQUFJLGtDQUFlLEVBQUUsQ0FBQTtZQUNoQyxLQUFLLE9BQU87Z0JBQ1IsTUFBTSxDQUFDLElBQUksb0NBQWdCLEVBQUUsQ0FBQTtZQUNqQztnQkFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLFlBQVksZUFBTSxDQUFDLENBQUMsQ0FBQztvQkFDekIsTUFBTSxDQUFDLElBQUksa0NBQWUsRUFBRSxDQUFBO2dCQUNoQyxDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLE1BQU0sSUFBSSxLQUFLLENBQUMsaUJBQWUsSUFBSSxvQkFBaUIsQ0FBQyxDQUFBO2dCQUN6RCxDQUFDO1FBQ1QsQ0FBQztJQUNMLENBQUM7SUFFTyxvQ0FBWSxHQUFwQixVQUFxQixHQUFXLEVBQUUsVUFBZ0MsRUFBRSxLQUFVLEVBQUUsT0FBMEI7UUFDdEcsSUFBTSxNQUFNLEdBQUcsSUFBSSxxREFBd0IsRUFBRSxDQUFBO1FBQzdDLElBQU0sSUFBSSxHQUFtQixVQUFVLENBQUMsSUFBSSxDQUFBO1FBQzVDLElBQU0sS0FBSyxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUE7UUFFakMsSUFBSSxTQUFTLEdBQWMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUVsRCxFQUFFLENBQUMsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNuQixNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFBO1lBRWpELEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDLENBQUM7Z0JBQ25CLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUE7Z0JBQ2xELE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUE7Z0JBRWxELHNEQUFzRDtnQkFDdEQsR0FBRyxDQUFDLENBQUMsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFLENBQUM7b0JBQ2hELE1BQU0sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBSSxHQUFHLFNBQUksS0FBTyxFQUFFLFVBQVUsRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQTtnQkFDeEYsQ0FBQztZQUVMLENBQUM7UUFDTCxDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixNQUFNLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQTtRQUNuRSxDQUFDO1FBRUQsTUFBTSxDQUFDLE1BQU0sQ0FBQTtJQUNqQixDQUFDO0lBRU0sZ0NBQVEsR0FBZixVQUFnQixHQUFXLEVBQUUsVUFBZ0MsRUFBRSxLQUFVLEVBQUUsT0FBMEI7UUFDakcsSUFBTSxNQUFNLEdBQUcsSUFBSSxxREFBd0IsRUFBRSxDQUFBO1FBQzdDLElBQU0sS0FBSyxHQUFHLGFBQWEsQ0FBQyxLQUFLLENBQUE7UUFFakMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxHQUFHLEVBQUUsVUFBVSxDQUFDLENBQUMsQ0FBQTtRQUNsRCxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sS0FBSyxLQUFLLFdBQVcsSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3ZFLE1BQU0sQ0FBQyxNQUFNLENBQUE7UUFDakIsQ0FBQztRQUVELE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUE7UUFDdkQsRUFBRSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLE1BQU0sQ0FBQyxNQUFNLENBQUE7UUFDakIsQ0FBQztRQUVELElBQU0sS0FBSyxHQUFxQixLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBSSxVQUFVLENBQUMsSUFBeUIsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFzQixDQUFDLENBQUE7UUFFNUksR0FBRyxDQUFDLENBQWEsVUFBSyxFQUFMLGVBQUssRUFBTCxtQkFBSyxFQUFMLElBQUssQ0FBQztZQUFsQixJQUFJLElBQUksY0FBQTtZQUNULElBQU0sZ0JBQWdCLEdBQUcsRUFBRSxVQUFJLEVBQUUsQ0FBQTtZQUNqQyxDQUFDLENBQUMsUUFBUSxDQUFDLGdCQUFnQixFQUFFLFVBQVUsQ0FBQyxDQUFBO1lBQ3hDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsZ0JBQWdCLEVBQUUsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUE7WUFFbkUsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFBQyxLQUFLLENBQUE7WUFBQyxDQUFDO1NBQ2xDO1FBRUQsTUFBTSxDQUFDLE1BQU0sQ0FBQTtJQUNqQixDQUFDO0lBRU0sNkJBQUssR0FBWixVQUFhLFVBQWdDLEVBQUUsS0FBVSxFQUFFLE9BQXFCLEVBQUUsTUFBVztRQUN6RixJQUFJLE1BQU0sR0FBUSxLQUFLLENBQUE7UUFFdkIsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLGtCQUFrQixJQUFJLE9BQU8sTUFBTSxLQUFLLFFBQVEsSUFBSSxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEYsRUFBRSxDQUFDLENBQUMsVUFBVSxDQUFDLFdBQVcsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxNQUFNLEdBQUcsSUFBSSxDQUFBO1lBQ2pCLENBQUM7UUFDTCxDQUFDO1FBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsSUFBSSxPQUFPLE1BQU0sS0FBSyxRQUFRLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVHLEVBQUUsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxXQUFXLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDbkMsTUFBTSxHQUFHLElBQUksQ0FBQTtZQUNqQixDQUFDO1FBQ0wsQ0FBQztRQUNELElBQU0sS0FBSyxHQUFxQixLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBSSxVQUFVLENBQUMsSUFBeUIsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFzQixDQUFDLENBQUE7UUFFNUksRUFBRSxDQUFDLENBQUMsT0FBTyxNQUFNLEtBQUssV0FBVyxJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ2xELEVBQUUsQ0FBQyxDQUFDLE9BQU8sVUFBVSxDQUFDLFlBQVksS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDO2dCQUNqRCxNQUFNLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLENBQUE7WUFDakQsQ0FBQztRQUNMLENBQUM7UUFFRCxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxJQUFJLE9BQU8sVUFBVSxDQUFDLFNBQVMsS0FBSyxVQUFVLENBQUMsQ0FBQyxDQUFDO1lBQ3RFLE1BQU0sR0FBRyxVQUFVLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQTtRQUNqRCxDQUFDO1FBRUQsR0FBRyxDQUFDLENBQWEsVUFBb0MsRUFBcEMsS0FBQSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBcUIsRUFBcEMsY0FBb0MsRUFBcEMsSUFBb0MsQ0FBQztZQUFqRCxJQUFJLElBQUksU0FBQTtZQUNULE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQTtTQUM5RTtRQUVELE1BQU0sQ0FBQyxNQUFNLENBQUE7SUFDakIsQ0FBQztJQTlQYSxtQkFBSyxHQUFHO1FBQ2xCLE9BQU8sRUFBRSxVQUFDLEtBQVUsRUFBRSxHQUFXLEVBQUUsVUFBZ0M7WUFDL0QsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssS0FBSyxXQUFXLElBQUksS0FBSyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzVFLE1BQU0sQ0FBQztvQkFDSCxRQUFRLEVBQUUsR0FBRztvQkFDYixJQUFJLEVBQUUsTUFBTTtvQkFDWixPQUFPLEVBQUUsY0FBWSxHQUFHLHlDQUFvQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQU07aUJBQ3JGLENBQUE7WUFDTCxDQUFDO1lBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQTtRQUNmLENBQUM7UUFDRCxRQUFRLEVBQUUsVUFBQyxLQUFVLEVBQUUsR0FBVyxFQUFFLFVBQWdDO1lBQ2hFLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxLQUFLLEtBQUssV0FBVyxJQUFJLEtBQUssS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sVUFBVSxDQUFDLFFBQVEsS0FBSyxRQUFRLENBQUMsSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUN0SSxNQUFNLENBQUM7b0JBQ0gsUUFBUSxFQUFFLEdBQUc7b0JBQ2IsSUFBSSxFQUFFLFVBQVU7b0JBQ2hCLE9BQU8sRUFBRSxjQUFZLEdBQUcseUNBQW9DLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSx1QkFBa0IsVUFBVSxDQUFDLFFBQVEsY0FBVztpQkFDbkksQ0FBQTtZQUNMLENBQUM7WUFFRCxNQUFNLENBQUMsSUFBSSxDQUFBO1FBQ2YsQ0FBQztRQUNELFFBQVEsRUFBRSxVQUFDLEtBQVUsRUFBRSxHQUFXLEVBQUUsVUFBZ0M7WUFDaEUsRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLEtBQUssS0FBSyxXQUFXLElBQUksS0FBSyxLQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxVQUFVLENBQUMsUUFBUSxLQUFLLFFBQVEsQ0FBQyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RJLE1BQU0sQ0FBQztvQkFDSCxRQUFRLEVBQUUsR0FBRztvQkFDYixJQUFJLEVBQUUsVUFBVTtvQkFDaEIsT0FBTyxFQUFFLGNBQVksR0FBRyx5Q0FBb0MsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLHFCQUFnQixVQUFVLENBQUMsUUFBUSxjQUFXO2lCQUNqSSxDQUFBO1lBQ0wsQ0FBQztZQUNELE1BQU0sQ0FBQyxJQUFJLENBQUE7UUFDZixDQUFDO1FBQ0QsUUFBUSxFQUFFLFVBQUMsS0FBVSxFQUFFLEdBQVcsRUFBRSxVQUFnQztZQUNoRSxFQUFFLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxRQUFRLElBQUksQ0FBQyxPQUFPLEtBQUssS0FBSyxXQUFXLElBQUksS0FBSyxJQUFJLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDMUUsTUFBTSxDQUFDO29CQUNILFFBQVEsRUFBRSxHQUFHO29CQUNiLElBQUksRUFBRSxVQUFVO29CQUNoQixPQUFPLEVBQUUsZ0NBQThCLEdBQUs7aUJBQy9DLENBQUE7WUFDTCxDQUFDO1lBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQTtRQUNmLENBQUM7UUFDRCxhQUFhLEVBQUUsVUFBQyxLQUFVLEVBQUUsR0FBVyxFQUFFLFVBQWdDO1lBQ3JFLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxLQUFLLEtBQUssV0FBVyxJQUFJLEtBQUssS0FBSyxJQUFJLENBQUMsSUFBSSxPQUFPLFVBQVUsQ0FBQyxhQUFhLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDdEcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssSUFBSSxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUN2QyxNQUFNLENBQUM7d0JBQ0gsUUFBUSxFQUFFLEdBQUc7d0JBQ2IsSUFBSSxFQUFFLGVBQWU7d0JBQ3JCLE9BQU8sRUFBRSxjQUFZLEdBQUcsNkJBQTBCO3FCQUNyRCxDQUFBO2dCQUNMLENBQUM7WUFDTCxDQUFDO1lBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQTtRQUNmLENBQUM7UUFDRCxNQUFNLEVBQUUsVUFBQyxLQUFVLEVBQUUsR0FBVyxFQUFFLFNBQStCLEVBQUUsTUFBVyxFQUFFLE9BQTBCLEVBQUUsTUFBZ0IsRUFBRSxJQUFZO1lBQ3RJLElBQU0sS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQTtZQUVwRCxFQUFFLENBQUMsQ0FBQyxPQUFPLEtBQUssS0FBSyxRQUFRLENBQUMsQ0FBQyxDQUFDO2dCQUM1QixNQUFNLENBQUM7b0JBQ0gsUUFBUSxFQUFFLEdBQUc7b0JBQ2IsSUFBSSxFQUFFLElBQUk7b0JBQ1YsT0FBTyxFQUFFLEtBQUs7aUJBQ2pCLENBQUE7WUFDTCxDQUFDO1lBQ0QsTUFBTSxDQUFDLElBQUksQ0FBQTtRQUNmLENBQUM7S0FDSixDQUFBO0lBNkxMLG9CQUFDO0FBQUQsQ0FqUUEsQUFpUUMsSUFBQTtBQWpRWSxxQkFBYSxnQkFpUXpCLENBQUEiLCJmaWxlIjoidmFsaWRhdG9ycy9yb290LXZhbGlkYXRvci5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJ1xuXG5pbXBvcnQge1xuICAgIFZhbGlkYXRvcixcbiAgICBWYWxpZGF0aW9uRGVmaW5pdGlvbixcbiAgICBEZWZpbml0aW9uVHlwZSxcbiAgICBWYWxpZGF0aW9uUmVzdWx0LFxuICAgIFZhbGlkYXRpb25PcHRpb25zLFxuICAgIFZhbGlkYXRpb25FcnJvcixcbiAgICBDbGVhbk9wdGlvbnNcbn0gZnJvbSAnLi4vaW50ZXJmYWNlcydcbmltcG9ydCB7IENvbXBvc2VkVmFsaWRhdGlvblJlc3VsdCB9IGZyb20gJy4uL2NvbXBvc2VkLXZhbGlkYXRpb24tcmVzdWx0J1xuaW1wb3J0IHsgU3RyaW5nVmFsaWRhdG9yIH0gZnJvbSAnLi9zdHJpbmctdmFsaWRhdG9yJ1xuaW1wb3J0IHsgTnVtYmVyVmFsaWRhdG9yIH0gZnJvbSAnLi9udW1iZXItdmFsaWRhdG9yJ1xuaW1wb3J0IHsgRGF0ZVZhbGlkYXRvciB9IGZyb20gJy4vZGF0ZS12YWxpZGF0b3InXG5pbXBvcnQgeyBPYmplY3RWYWxpZGF0b3IgfSBmcm9tICcuL29iamVjdC12YWxpZGF0b3InXG5pbXBvcnQgeyBTY2hlbWFWYWxpZGF0b3IgfSBmcm9tICcuL3NjaGVtYS12YWxpZGF0b3InXG5pbXBvcnQgeyBCb29sZWFuVmFsaWRhdG9yIH0gZnJvbSAnLi9ib29sZWFuLXZhbGlkYXRvcidcbmltcG9ydCB7IFNjaGVtYSB9IGZyb20gJy4uL3NjaGVtYSdcbmltcG9ydCB7IGNsZWFuZWQgfSBmcm9tICcuLi9jbGVhbmVkJ1xuXG5leHBvcnQgY2xhc3MgUm9vdFZhbGlkYXRvciBpbXBsZW1lbnRzIFZhbGlkYXRvciB7XG5cbiAgICBwdWJsaWMgc3RhdGljIFJVTEVTID0ge1xuICAgICAgICBpc0FycmF5OiAodmFsdWU6IGFueSwga2V5OiBzdHJpbmcsIGRlZmluaXRpb246IFZhbGlkYXRpb25EZWZpbml0aW9uKTogVmFsaWRhdGlvbkVycm9yIHwgbnVsbCA9PiB7XG4gICAgICAgICAgICBpZiAoKHR5cGVvZiB2YWx1ZSAhPT0gJ3VuZGVmaW5lZCcgJiYgdmFsdWUgIT09IG51bGwpICYmICFBcnJheS5pc0FycmF5KHZhbHVlKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgIHByb3BlcnR5OiBrZXksXG4gICAgICAgICAgICAgICAgICAgIHJ1bGU6ICd0eXBlJyxcbiAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogYFByb3BlcnR5ICR7a2V5fSBleHBlY3RlZCB0byBiZSBhbiBhcnJheSBvZiB0eXBlICR7ZGVmaW5pdGlvbi50eXBlLm5hbWV9YFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBudWxsXG4gICAgICAgIH0sXG4gICAgICAgIG1pbkNvdW50OiAodmFsdWU6IGFueSwga2V5OiBzdHJpbmcsIGRlZmluaXRpb246IFZhbGlkYXRpb25EZWZpbml0aW9uKTogVmFsaWRhdGlvbkVycm9yIHwgbnVsbCA9PiB7XG4gICAgICAgICAgICBpZiAoKHR5cGVvZiB2YWx1ZSAhPT0gJ3VuZGVmaW5lZCcgJiYgdmFsdWUgIT09IG51bGwpICYmICh0eXBlb2YgZGVmaW5pdGlvbi5taW5Db3VudCA9PT0gJ251bWJlcicpICYmIHZhbHVlLmxlbmd0aCA8IGRlZmluaXRpb24ubWluQ291bnQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICBwcm9wZXJ0eToga2V5LFxuICAgICAgICAgICAgICAgICAgICBydWxlOiAnbWluQ291bnQnLFxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBgUHJvcGVydHkgJHtrZXl9IGV4cGVjdGVkIHRvIGJlIGFuIGFycmF5IG9mIHR5cGUgJHtkZWZpbml0aW9uLnR5cGUubmFtZX0gd2l0aCBhdCBsZWFzdCAke2RlZmluaXRpb24ubWluQ291bnR9IGVsZW1lbnRzYFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgcmV0dXJuIG51bGxcbiAgICAgICAgfSxcbiAgICAgICAgbWF4Q291bnQ6ICh2YWx1ZTogYW55LCBrZXk6IHN0cmluZywgZGVmaW5pdGlvbjogVmFsaWRhdGlvbkRlZmluaXRpb24pOiBWYWxpZGF0aW9uRXJyb3IgfCBudWxsID0+IHtcbiAgICAgICAgICAgIGlmICgodHlwZW9mIHZhbHVlICE9PSAndW5kZWZpbmVkJyAmJiB2YWx1ZSAhPT0gbnVsbCkgJiYgKHR5cGVvZiBkZWZpbml0aW9uLm1heENvdW50ID09PSAnbnVtYmVyJykgJiYgdmFsdWUubGVuZ3RoID4gZGVmaW5pdGlvbi5tYXhDb3VudCkge1xuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgIHByb3BlcnR5OiBrZXksXG4gICAgICAgICAgICAgICAgICAgIHJ1bGU6ICdtYXhDb3VudCcsXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IGBQcm9wZXJ0eSAke2tleX0gZXhwZWN0ZWQgdG8gYmUgYW4gYXJyYXkgb2YgdHlwZSAke2RlZmluaXRpb24udHlwZS5uYW1lfSB3aXRoIGF0IG1heCAke2RlZmluaXRpb24ubWF4Q291bnR9IGVsZW1lbnRzYFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBudWxsXG4gICAgICAgIH0sXG4gICAgICAgIHJlcXVpcmVkOiAodmFsdWU6IGFueSwga2V5OiBzdHJpbmcsIGRlZmluaXRpb246IFZhbGlkYXRpb25EZWZpbml0aW9uKTogVmFsaWRhdGlvbkVycm9yIHwgbnVsbCA9PiB7XG4gICAgICAgICAgICBpZiAoIWRlZmluaXRpb24ub3B0aW9uYWwgJiYgKHR5cGVvZiB2YWx1ZSA9PT0gJ3VuZGVmaW5lZCcgfHwgdmFsdWUgPT0gbnVsbCkpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAgICAgICBwcm9wZXJ0eToga2V5LFxuICAgICAgICAgICAgICAgICAgICBydWxlOiAncmVxdWlyZWQnLFxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiBgTWlzc2luZyB2YWx1ZSBmb3IgcHJvcGVydHkgJHtrZXl9YFxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBudWxsXG4gICAgICAgIH0sXG4gICAgICAgIGFsbG93ZWRWYWx1ZXM6ICh2YWx1ZTogYW55LCBrZXk6IHN0cmluZywgZGVmaW5pdGlvbjogVmFsaWRhdGlvbkRlZmluaXRpb24pOiBWYWxpZGF0aW9uRXJyb3IgfCBudWxsID0+IHtcbiAgICAgICAgICAgIGlmICgodHlwZW9mIHZhbHVlICE9PSAndW5kZWZpbmVkJyAmJiB2YWx1ZSAhPT0gbnVsbCkgJiYgdHlwZW9mIGRlZmluaXRpb24uYWxsb3dlZFZhbHVlcyAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICBpZiAoISh2YWx1ZSBpbiBkZWZpbml0aW9uLmFsbG93ZWRWYWx1ZXMpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBwcm9wZXJ0eToga2V5LFxuICAgICAgICAgICAgICAgICAgICAgICAgcnVsZTogJ2FsbG93ZWRWYWx1ZXMnLFxuICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogYFZhbHVlIG9mICR7a2V5fSBpcyBub3QgaW4gYWxsb3dlZFZhbHVlc2BcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBudWxsXG4gICAgICAgIH0sXG4gICAgICAgIGN1c3RvbTogKHZhbHVlOiBhbnksIGtleTogc3RyaW5nLCBkZWZpbnRpb246IFZhbGlkYXRpb25EZWZpbml0aW9uLCBvYmplY3Q6IGFueSwgb3B0aW9uczogVmFsaWRhdGlvbk9wdGlvbnMsIGN1c3RvbTogRnVuY3Rpb24sIHJ1bGU6IHN0cmluZyk6IFZhbGlkYXRpb25FcnJvciB8IG51bGwgPT4ge1xuICAgICAgICAgICAgY29uc3QgZXJyb3IgPSBjdXN0b20odmFsdWUsIG9iamVjdCwgb3B0aW9ucy5jb250ZXh0KVxuXG4gICAgICAgICAgICBpZiAodHlwZW9mIGVycm9yID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgICAgIHByb3BlcnR5OiBrZXksXG4gICAgICAgICAgICAgICAgICAgIHJ1bGU6IHJ1bGUsXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IGVycm9yXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG51bGxcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBzdGF0aWMgZ2V0VmFsaWRhdG9yc0ZvcktleShrZXk6IHN0cmluZywgZGVmaW5pdGlvbjogVmFsaWRhdGlvbkRlZmluaXRpb24sIG9wdGlvbnM6IFZhbGlkYXRpb25PcHRpb25zLCBvYmplY3Q/OiBhbnkpIHtcbiAgICAgICAgY29uc3QgdmFsaWRhdG9yczogYW55ID0ge31cblxuICAgICAgICBpZiAoIWRlZmluaXRpb24ub3B0aW9uYWwpIHtcbiAgICAgICAgICAgIHZhbGlkYXRvcnMucmVxdWlyZWQgPSBjbGVhbmVkKFJvb3RWYWxpZGF0b3IuUlVMRVMucmVxdWlyZWQsIGtleSwgZGVmaW5pdGlvbiwgb3B0aW9ucylcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChkZWZpbml0aW9uLmFsbG93ZWRWYWx1ZXMpIHtcbiAgICAgICAgICAgIHZhbGlkYXRvcnMuYWxsb3dlZFZhbHVlcyA9IGNsZWFuZWQoUm9vdFZhbGlkYXRvci5SVUxFUy5hbGxvd2VkVmFsdWVzLCBrZXksIGRlZmluaXRpb24sIG9wdGlvbnMpXG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZGVmaW5pdGlvbi5hcnJheSkge1xuICAgICAgICAgICAgdmFsaWRhdG9ycy5pc0FycmF5ID0gY2xlYW5lZChSb290VmFsaWRhdG9yLlJVTEVTLmlzQXJyYXksIGtleSwgZGVmaW5pdGlvbiwgb3B0aW9ucylcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChkZWZpbml0aW9uLm1pbkNvdW50KSB7XG4gICAgICAgICAgICB2YWxpZGF0b3JzLm1pbkNvdW50ID0gY2xlYW5lZChSb290VmFsaWRhdG9yLlJVTEVTLm1pbkNvdW50LCBrZXksIGRlZmluaXRpb24sIG9wdGlvbnMpXG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZGVmaW5pdGlvbi5tYXhDb3VudCkge1xuICAgICAgICAgICAgdmFsaWRhdG9ycy5tYXhDb3VudCA9IGNsZWFuZWQoUm9vdFZhbGlkYXRvci5SVUxFUy5tYXhDb3VudCwga2V5LCBkZWZpbml0aW9uLCBvcHRpb25zKVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGRlZmluaXRpb24uY3VzdG9tKSAge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBkZWZpbml0aW9uLmN1c3RvbSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgIHZhbGlkYXRvcnMuY3VzdG9tID0gY2xlYW5lZChSb290VmFsaWRhdG9yLlJVTEVTLmN1c3RvbSwga2V5LCBkZWZpbml0aW9uLCBvcHRpb25zLCBvYmplY3QsIGRlZmluaXRpb24uY3VzdG9tLCAnY3VzdG9tJylcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGRlZmluaXRpb24uY3VzdG9tID09PSAnb2JqZWN0Jykge1xuICAgICAgICAgICAgICAgIGZvciAobGV0IHJ1bGUgaW4gZGVmaW5pdGlvbi5jdXN0b20pIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGRlZmluaXRpb24uY3VzdG9tLmhhc093blByb3BlcnR5KHJ1bGUpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2YWxpZGF0b3JzW3J1bGVdID0gY2xlYW5lZChSb290VmFsaWRhdG9yLlJVTEVTLmN1c3RvbSwga2V5LCBkZWZpbml0aW9uLCBvcHRpb25zLCBvYmplY3QsIGRlZmluaXRpb24uY3VzdG9tW3J1bGVdLCBydWxlKVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgc3dpdGNoIChkZWZpbml0aW9uLnR5cGUpIHtcbiAgICAgICAgY2FzZSBCb29sZWFuOlxuICAgICAgICAgICAgXy5hc3NpZ24odmFsaWRhdG9ycywgQm9vbGVhblZhbGlkYXRvci5nZXRWYWxpZGF0b3JzRm9yS2V5KGtleSwgZGVmaW5pdGlvbiwgb3B0aW9ucywgb2JqZWN0KSlcbiAgICAgICAgICAgIGJyZWFrXG4gICAgICAgIGNhc2UgRGF0ZTpcbiAgICAgICAgICAgIF8uYXNzaWduKHZhbGlkYXRvcnMsIERhdGVWYWxpZGF0b3IuZ2V0VmFsaWRhdG9yc0ZvcktleShrZXksIGRlZmluaXRpb24sIG9wdGlvbnMsIG9iamVjdCkpXG4gICAgICAgICAgICBicmVha1xuICAgICAgICBjYXNlIE51bWJlcjpcbiAgICAgICAgICAgIF8uYXNzaWduKHZhbGlkYXRvcnMsIE51bWJlclZhbGlkYXRvci5nZXRWYWxpZGF0b3JzRm9yS2V5KGtleSwgZGVmaW5pdGlvbiwgb3B0aW9ucywgb2JqZWN0KSlcbiAgICAgICAgICAgIGJyZWFrXG4gICAgICAgIGNhc2UgT2JqZWN0OlxuICAgICAgICAgICAgXy5hc3NpZ24odmFsaWRhdG9ycywgT2JqZWN0VmFsaWRhdG9yLmdldFZhbGlkYXRvcnNGb3JLZXkoa2V5LCBkZWZpbml0aW9uLCBvcHRpb25zLCBvYmplY3QpKVxuICAgICAgICAgICAgYnJlYWtcbiAgICAgICAgY2FzZSBTdHJpbmc6XG4gICAgICAgICAgICBfLmFzc2lnbih2YWxpZGF0b3JzLCBTdHJpbmdWYWxpZGF0b3IuZ2V0VmFsaWRhdG9yc0ZvcktleShrZXksIGRlZmluaXRpb24sIG9wdGlvbnMsIG9iamVjdCkpXG4gICAgICAgICAgICBicmVha1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgaWYgKGRlZmluaXRpb24udHlwZSBpbnN0YW5jZW9mIFNjaGVtYSkge1xuICAgICAgICAgICAgICAgIF8uYXNzaWduKHZhbGlkYXRvcnMsIFNjaGVtYVZhbGlkYXRvci5nZXRWYWxpZGF0b3JzRm9yS2V5KGtleSwgZGVmaW5pdGlvbiwgb3B0aW9ucywgb2JqZWN0KSlcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBVbmtvd24gdHlwZSAke2RlZmluaXRpb24udHlwZX0gdXNlZCBpbiBzY2hlbWFgKVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB2YWxpZGF0b3JzXG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyBnZXRWYWxpZGF0b3IodHlwZTogRGVmaW5pdGlvblR5cGUpOiB7IG5ldyguLi5hcmdzOiBhbnlbXSk6IFZhbGlkYXRvciB9IHtcbiAgICAgICAgc3dpdGNoICh0eXBlKSB7XG4gICAgICAgICAgICBjYXNlIFN0cmluZzpcbiAgICAgICAgICAgICAgICByZXR1cm4gU3RyaW5nVmFsaWRhdG9yXG4gICAgICAgICAgICBjYXNlIE51bWJlcjpcbiAgICAgICAgICAgICAgICByZXR1cm4gTnVtYmVyVmFsaWRhdG9yXG4gICAgICAgICAgICBjYXNlIERhdGU6XG4gICAgICAgICAgICAgICAgcmV0dXJuIERhdGVWYWxpZGF0b3JcbiAgICAgICAgICAgIGNhc2UgT2JqZWN0OlxuICAgICAgICAgICAgICAgIHJldHVybiBPYmplY3RWYWxpZGF0b3JcbiAgICAgICAgICAgIGNhc2UgQm9vbGVhbjpcbiAgICAgICAgICAgICAgICByZXR1cm4gQm9vbGVhblZhbGlkYXRvclxuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICBpZiAodHlwZSBpbnN0YW5jZW9mIFNjaGVtYSkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gU2NoZW1hVmFsaWRhdG9yXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBVbmtvd24gdHlwZSAke3R5cGV9IHVzZWQgaW4gc2NoZW1hYClcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGdldFZhbGlkYXRvcih0eXBlOiBEZWZpbml0aW9uVHlwZSk6IFZhbGlkYXRvciB7XG4gICAgICAgIHN3aXRjaCAodHlwZSkge1xuICAgICAgICAgICAgY2FzZSBTdHJpbmc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBTdHJpbmdWYWxpZGF0b3IoKVxuICAgICAgICAgICAgY2FzZSBOdW1iZXI6XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBOdW1iZXJWYWxpZGF0b3IoKVxuICAgICAgICAgICAgY2FzZSBEYXRlOlxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgRGF0ZVZhbGlkYXRvcigpXG4gICAgICAgICAgICBjYXNlIE9iamVjdDpcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IE9iamVjdFZhbGlkYXRvcigpXG4gICAgICAgICAgICBjYXNlIEJvb2xlYW46XG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBCb29sZWFuVmFsaWRhdG9yKClcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgaWYgKHR5cGUgaW5zdGFuY2VvZiBTY2hlbWEpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBTY2hlbWFWYWxpZGF0b3IoKVxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgVW5rb3duIHR5cGUgJHt0eXBlfSB1c2VkIGluIHNjaGVtYWApXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSB2YWxpZGF0ZVR5cGUoa2V5OiBzdHJpbmcsIGRlZmluaXRpb246IFZhbGlkYXRpb25EZWZpbml0aW9uLCB2YWx1ZTogYW55LCBvcHRpb25zOiBWYWxpZGF0aW9uT3B0aW9ucyk6IFZhbGlkYXRpb25SZXN1bHQge1xuICAgICAgICBjb25zdCByZXN1bHQgPSBuZXcgQ29tcG9zZWRWYWxpZGF0aW9uUmVzdWx0KClcbiAgICAgICAgY29uc3QgdHlwZTogRGVmaW5pdGlvblR5cGUgPSBkZWZpbml0aW9uLnR5cGVcbiAgICAgICAgY29uc3QgcnVsZXMgPSBSb290VmFsaWRhdG9yLlJVTEVTXG5cbiAgICAgICAgbGV0IHZhbGlkYXRvcjogVmFsaWRhdG9yID0gdGhpcy5nZXRWYWxpZGF0b3IodHlwZSlcblxuICAgICAgICBpZiAoZGVmaW5pdGlvbi5hcnJheSkge1xuICAgICAgICAgICAgcmVzdWx0LmFuZChydWxlcy5pc0FycmF5KHZhbHVlLCBrZXksIGRlZmluaXRpb24pKVxuXG4gICAgICAgICAgICBpZiAocmVzdWx0LmlzVmFsaWQoKSkge1xuICAgICAgICAgICAgICAgIHJlc3VsdC5hbmQocnVsZXMubWluQ291bnQodmFsdWUsIGtleSwgZGVmaW5pdGlvbikpXG4gICAgICAgICAgICAgICAgcmVzdWx0LmFuZChydWxlcy5tYXhDb3VudCh2YWx1ZSwga2V5LCBkZWZpbml0aW9uKSlcblxuICAgICAgICAgICAgICAgIC8vIHVzZSBjbGFzc2ljIGZvciBsb29wIGhlcmUgYmVjYXVzZSB3ZSBuZWVkIHRoZSBpbmRleFxuICAgICAgICAgICAgICAgIGZvciAobGV0IGluZGV4ID0gMDsgaW5kZXggPCB2YWx1ZS5sZW5ndGg7IGluZGV4KyspIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0LmFuZCh2YWxpZGF0b3IudmFsaWRhdGUoYCR7a2V5fS4ke2luZGV4fWAsIGRlZmluaXRpb24sIHZhbHVlW2luZGV4XSwgb3B0aW9ucykpXG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXN1bHQuYW5kKHZhbGlkYXRvci52YWxpZGF0ZShrZXksIGRlZmluaXRpb24sIHZhbHVlLCBvcHRpb25zKSlcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiByZXN1bHRcbiAgICB9XG5cbiAgICBwdWJsaWMgdmFsaWRhdGUoa2V5OiBzdHJpbmcsIGRlZmluaXRpb246IFZhbGlkYXRpb25EZWZpbml0aW9uLCB2YWx1ZTogYW55LCBvcHRpb25zOiBWYWxpZGF0aW9uT3B0aW9ucyk6IFZhbGlkYXRpb25SZXN1bHQge1xuICAgICAgICBjb25zdCByZXN1bHQgPSBuZXcgQ29tcG9zZWRWYWxpZGF0aW9uUmVzdWx0KClcbiAgICAgICAgY29uc3QgcnVsZXMgPSBSb290VmFsaWRhdG9yLlJVTEVTXG5cbiAgICAgICAgcmVzdWx0LmFuZChydWxlcy5yZXF1aXJlZCh2YWx1ZSwga2V5LCBkZWZpbml0aW9uKSlcbiAgICAgICAgaWYgKCFyZXN1bHQuaXNWYWxpZCgpIHx8ICh0eXBlb2YgdmFsdWUgPT09ICd1bmRlZmluZWQnIHx8IHZhbHVlID09IG51bGwpKSB7XG4gICAgICAgICAgICByZXR1cm4gcmVzdWx0XG4gICAgICAgIH1cblxuICAgICAgICByZXN1bHQuYW5kKHJ1bGVzLmFsbG93ZWRWYWx1ZXModmFsdWUsIGtleSwgZGVmaW5pdGlvbikpXG4gICAgICAgIGlmICghcmVzdWx0LmlzVmFsaWQoKSkge1xuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgdHlwZXM6IERlZmluaXRpb25UeXBlW10gPSBBcnJheS5pc0FycmF5KGRlZmluaXRpb24udHlwZSkgPyAoZGVmaW5pdGlvbi50eXBlIGFzIERlZmluaXRpb25UeXBlW10pIDogW2RlZmluaXRpb24udHlwZSBhcyBEZWZpbml0aW9uVHlwZV1cblxuICAgICAgICBmb3IgKGxldCB0eXBlIG9mIHR5cGVzKSB7XG4gICAgICAgICAgICBjb25zdCBzaW5nbGVEZWZpbml0aW9uID0geyB0eXBlIH1cbiAgICAgICAgICAgIF8uZGVmYXVsdHMoc2luZ2xlRGVmaW5pdGlvbiwgZGVmaW5pdGlvbilcbiAgICAgICAgICAgIHJlc3VsdC5vcih0aGlzLnZhbGlkYXRlVHlwZShrZXksIHNpbmdsZURlZmluaXRpb24sIHZhbHVlLCBvcHRpb25zKSlcblxuICAgICAgICAgICAgaWYgKHJlc3VsdC5pc1ZhbGlkKCkpIHsgYnJlYWsgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cblxuICAgIHB1YmxpYyBjbGVhbihkZWZpbml0aW9uOiBWYWxpZGF0aW9uRGVmaW5pdGlvbiwgdmFsdWU6IGFueSwgb3B0aW9uczogQ2xlYW5PcHRpb25zLCBvYmplY3Q6IGFueSk6IGFueSB7XG4gICAgICAgIGxldCByZXN1bHQ6IGFueSA9IHZhbHVlXG5cbiAgICAgICAgaWYgKG9wdGlvbnMucmVtb3ZlRW1wdHlTdHJpbmdzICYmIHR5cGVvZiByZXN1bHQgPT09ICdzdHJpbmcnICYmIHZhbHVlLnRyaW0oKS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIGlmIChkZWZpbml0aW9uLnJlbW92ZUVtcHR5ICE9PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIHJlc3VsdCA9IG51bGxcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIGlmIChvcHRpb25zLnJlbW92ZUVtcHR5T2JqZWN0cyAmJiB0eXBlb2YgcmVzdWx0ID09PSAnb2JqZWN0JyAmJiBfLmlzRW1wdHkocmVzdWx0KSAmJiAhXy5pc0RhdGUocmVzdWx0KSkge1xuICAgICAgICAgICAgaWYgKGRlZmluaXRpb24ucmVtb3ZlRW1wdHkgIT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gbnVsbFxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGNvbnN0IHR5cGVzOiBEZWZpbml0aW9uVHlwZVtdID0gQXJyYXkuaXNBcnJheShkZWZpbml0aW9uLnR5cGUpID8gKGRlZmluaXRpb24udHlwZSBhcyBEZWZpbml0aW9uVHlwZVtdKSA6IFtkZWZpbml0aW9uLnR5cGUgYXMgRGVmaW5pdGlvblR5cGVdXG5cbiAgICAgICAgaWYgKHR5cGVvZiByZXN1bHQgPT09ICd1bmRlZmluZWQnIHx8IHJlc3VsdCA9PSBudWxsKSB7XG4gICAgICAgICAgICBpZiAodHlwZW9mIGRlZmluaXRpb24uZGVmYXVsdFZhbHVlICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgIHJlc3VsdCA9IF8uY2xvbmVEZWVwKGRlZmluaXRpb24uZGVmYXVsdFZhbHVlKVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKG9wdGlvbnMuZ2V0QXV0b1ZhbHVlcyAmJiB0eXBlb2YgZGVmaW5pdGlvbi5hdXRvVmFsdWUgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IGRlZmluaXRpb24uYXV0b1ZhbHVlKHJlc3VsdCwgb2JqZWN0KVxuICAgICAgICB9XG5cbiAgICAgICAgZm9yIChsZXQgdHlwZSBvZiBfLnJldmVyc2UodHlwZXMpIGFzIERlZmluaXRpb25UeXBlW10pIHtcbiAgICAgICAgICAgIHJlc3VsdCA9IHRoaXMuZ2V0VmFsaWRhdG9yKHR5cGUpLmNsZWFuKGRlZmluaXRpb24sIHJlc3VsdCwgb3B0aW9ucywgb2JqZWN0KVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHJlc3VsdFxuICAgIH1cbn1cbiJdfQ==
